class Ocz extends HTMLElement {
    constructor() {
        super();

        this.r = 25;

        const shadowRoot = this.attachShadow({mode: 'open'});
        shadowRoot.innerHTML = `
            <style>
                :host {
                    display: inline-block;
                }   
                .eye {
                    fill: var(--ocz-eye-color, #fff);
                    stroke: var(--ocz-eye-stroke, #000);
                    stroke-width: var(--ocz-eye-stroke-width, 1);
                }
                .iris {
                    fill: var(--ocz-iris-color, #000);
                    stroke: var(--ocz-iris-stroke, #000);
                    stroke-width: var(--ocz-iris-stroke-width, 1);
                }
            </style>

            <svg viewBox="0 0 100 100">
                <ellipse class="eye" cx="50" cy="50" rx="49" ry="49"/>
                <ellipse class="iris" cx="50" cy="50" rx="${this.r}" ry="${this.r}"/>
            </svg>
        `;

        this.iris = shadowRoot.querySelector('.iris');

        document.addEventListener('mousemove', (e) => {
            this.render(e.clientX, e.clientY);
        });
    }

    static get observedAttributes() {
        return ['r'];
    }

    attributeChangedCallback(attr, oldVal, newVal) {
        switch(attr) {
            case 'r':
                this.r = Math.max(3, Math.min(newVal, 45));
                this.render();
        }
    }

    render(mouseX = null, mouseY = null) {
        this.iris.setAttribute('rx', this.r);
        this.iris.setAttribute('ry', this.r);

        if (mouseX === null || mouseY === null) {
            this.iris.setAttribute('cx', 50);
            this.iris.setAttribute('cy', 50);
            return;
        }

        const orbit = 50 - this.r;

        const viewportOffset = this.getBoundingClientRect();

        const x = mouseX - viewportOffset.left - viewportOffset.width / 2;
        const y = mouseY - viewportOffset.top - viewportOffset.width / 2;
        const z = Math.sqrt(x ** 2 + y ** 2);

        const irisX = z <= orbit ? x : x / (z / orbit);
        const irisY = z <= orbit ? y : y / (z / orbit);

        this.iris.setAttribute('cx', 50 + irisX);
        this.iris.setAttribute('cy', 50 + irisY);
    }
}

window.customElements.define('avris-ocz', Ocz);
