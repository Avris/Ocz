# Ocz – Just some googly eyes

Fair warning: this library is pretty useless. And only moderately fun... 🤷‍

But it seemed like a cool way to learn a bit about web components, so here it is:
a component that creates googly eyes that follow your mouse.


(browser support is not perfect and the whole concept doesn't make much sense of mobile, so if it doesn't look right for you, well, tough luck 🤷‍)

 - [Demo](https://ocz.avris.it)
 - [Source code](https://gitlab.com/Avris/Ocz) 
 - [NPM](https://www.npmjs.com/package/avris-sorteocz) 

## Installation

You can install Ocz as a node module:

    $ npm i --save avris-ocz
    
    or
    
    $ yarn add avris-ocz

Or use a CDN:

    <script src="https://glcdn.githack.com/Avris/Ocz/raw/v0.0.1/src/ocz.js"></script>
    
## Usage

Check out the [demo](https://ocz.avris.it).

## Copyright
 
 * **Author:** Andrea Vos [(avris.it)](https://avris.it)
 * **Licence:** [OQL](https://oql.avris.it/license?c=Andrea Vos|https://avris.it)
 